export function getUniqueYears(data: { year: number }[]) {
  const uniqueYearsSet = new Set();
  const uniqueYearsArray = [];

  for (const item of data) {
    if (!uniqueYearsSet.has(item.year)) {
      uniqueYearsSet.add(item.year);
      uniqueYearsArray.push({ year: item.year });
    }
  }

  return uniqueYearsArray;
}
