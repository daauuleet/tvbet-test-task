export default {
  handle: (inputString: string | number) => {
    const groups = String(inputString).match(/\d{1,3}(?=(\d{3})*$)/g);
    const result = groups?.join(" ");
    return result;
  },
};
