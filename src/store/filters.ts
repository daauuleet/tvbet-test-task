import { defineStore } from "pinia";
import { computed, ref, watch } from "vue";
import { useProductsStore } from "./products.ts";
import { getUniqueYears } from "../helpers/removeDuplicates.ts";

interface IFilters {
  brands: number[];
  years: number[];
  store1Checked: boolean;
  store2Checked: boolean;
}

export const useFiltersStore = defineStore("filters", () => {
  const productsStore = useProductsStore();

  const filters = ref<IFilters>({
    brands: [],
    years: [],
    store1Checked: false,
    store2Checked: false,
  });

  const yearsList = computed(() => {
    const allYears = productsStore.catalog.map((item) => {
      return {
        year: item.year,
      };
    });
    return getUniqueYears(allYears).sort((a, b) => a.year - b.year);
  });

  watch(
    () => filters,
    () => {
      handleFilter(
        filters.value.years,
        filters.value.brands,
        filters.value.store1Checked,
        filters.value.store2Checked,
      );
    },
    { deep: true },
  );

  const handleFilter = (
    years: number[],
    brands: number[],
    store1Checked: boolean,
    store2Checked: boolean,
  ): void => {
    productsStore.filteredCatalog = productsStore.catalog.filter((item) => {
      return (
        (years.length === 0 || years.includes(item.year)) &&
        (brands.length === 0 || brands.includes(item.brand)) &&
        (!store1Checked || item.st1 > 0) &&
        (!store2Checked || item.st2 > 0)
      );
    });
  };

  return {
    yearsList,
    filters,
  };
});
