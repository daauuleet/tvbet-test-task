import { defineStore } from "pinia";
import { ref } from "vue";

export const useCurrenciesStore = defineStore("currencies", () => {
  const currentCurrency = ref<"usd" | "rub">("rub");

  const convert = (price: number): number => {
    return currentCurrency.value === "usd" ? Math.round(price * 0.011) : price;
  };

  return {
    currentCurrency,
    convert,
  };
});
