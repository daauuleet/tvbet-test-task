import { defineStore } from "pinia";
import { ref } from "vue";
import axios from "axios";
import { ICatalog, IBrand, IStock } from "../types";

export const useProductsStore = defineStore("products", () => {
  const catalog = ref<ICatalog[]>([]);
  const brands = ref<IBrand[]>([]);

  const stock = ref<IStock[]>([]);
  const filteredCatalog = ref<ICatalog[]>([]);

  const getCatalog = async () => {
    try {
      const { data } = await axios.get("/src/const/catalog.json");
      catalog.value = extendCatalogWithStock(data);
      filteredCatalog.value = catalog.value;
    } catch (error) {
      console.error(error);
    }
  };

  const getBrands = async () => {
    try {
      const { data } = await axios.get("/src/const/brands.json");
      brands.value = data as IBrand[];
    } catch (error) {
      console.error(error);
    }
  };

  const getStock = async () => {
    try {
      const { data } = await axios.get("/src/const/stock.json");
      stock.value = data as IStock[];
    } catch (error) {
      console.error(error);
    }
  };

  const extendCatalogWithStock = (_catalog: ICatalog[]): ICatalog[] => {
    return _catalog.map((item: ICatalog) => ({
      ...item,
      st1: stock.value.find((s) => s.art === item.art)?.st1 as number,
      st2: stock.value.find((s) => s.art === item.art)?.st2 as number,
    }));
  };

  const getBrandName = (brandId: number) => {
    if (brands.value) {
      return brands.value.find((brand) => brand.id == brandId)?.name;
    }
  };

  return {
    catalog,
    filteredCatalog,
    brands,
    stock,
    getCatalog,
    getBrands,
    getStock,
    getBrandName,
  };
});
