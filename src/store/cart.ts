import { defineStore } from "pinia";
import { computed, ref } from "vue";
import { ICatalog } from "../types";

interface ICart extends ICatalog {
  quantity: number;
}

export const useCartStore = defineStore("cart", () => {
  const cart = ref<ICart[]>(
    localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart") as string) : [],
  );

  const cartLength = computed(() => (cart.value ? cart.value.length : 0));

  const addToCart = (product: ICatalog) => {
    const existingProduct = cart.value.find((item: ICart) => item.art === product.art);

    if (existingProduct) {
      existingProduct.quantity += 1;
    } else {
      cart.value.push({ ...product, quantity: 1 });
    }
    localStorage.setItem("cart", JSON.stringify(cart.value));
  };

  const clearCart = () => {
    cart.value = [];
    localStorage.removeItem("cart");
  };

  const increaseQuantity = (product: ICart) => {
    const currentProduct = cart.value.find((item) => item.art === product.art);
    if (currentProduct) {
      currentProduct.quantity += 1;
      localStorage.setItem("cart", JSON.stringify(cart.value));
    }
  };

  const decreaseQuantity = (product: ICart) => {
    const currentProduct = cart.value.find((item) => item.art === product.art);
    if (currentProduct) {
      currentProduct.quantity -= 1;
      localStorage.setItem("cart", JSON.stringify(cart.value));
    }
  };

  return {
    cart,
    cartLength,
    addToCart,
    clearCart,
    increaseQuantity,
    decreaseQuantity,
  };
});
