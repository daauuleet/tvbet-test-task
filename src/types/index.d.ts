export interface ICatalog {
  name: string;
  art: string;
  brand: number;
  price: number;
  year: number;
  st1: number;
  st2: number;
}

export interface IBrand {
  name: string;
  id: number;
}

export interface IStock {
  art: string;
  st1: number;
  st2: number;
}
